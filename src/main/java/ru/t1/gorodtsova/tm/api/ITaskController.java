package ru.t1.gorodtsova.tm.api;

public interface ITaskController {

    void createTask();

    void showTask();

    void clearTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}