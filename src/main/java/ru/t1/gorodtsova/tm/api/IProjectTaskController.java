package ru.t1.gorodtsova.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
