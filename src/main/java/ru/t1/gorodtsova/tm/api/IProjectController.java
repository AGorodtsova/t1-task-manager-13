package ru.t1.gorodtsova.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
